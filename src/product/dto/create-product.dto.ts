import { IsNotEmpty, Length } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;
  @IsNotEmpty()
  price: string;
}
